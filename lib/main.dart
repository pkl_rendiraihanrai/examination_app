import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(MyApp());
}

class AppColors {
  static const Color purple = Color(0xff5C67B7);
  static const Color white = Color(0xffFFFFFF);
  static const Color baseColor = Color(0xffE7ECEF);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: AppColors.baseColor,
        body: ListView(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
              child: Column(
                children: [
                  TopNavigationBar(),
                  BreadcrumbsNavigation(),
                  ExaminationTitle(),
                  SoalPageViewBuilder(mySoal: mySoal),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ExaminationTitle extends StatelessWidget {
  const ExaminationTitle({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8),
      child: Text(
        'Ujian Perkalian Pertambahan & Pengurangan',
        style: GoogleFonts.poppins(
          fontSize: 36,
          fontWeight: FontWeight.w700,
          color: AppColors.purple,
          height: 1.5,
        ),
      ),
    );
  }
}

class BreadcrumbsNavigation extends StatelessWidget {
  const BreadcrumbsNavigation({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24),
      child: Text(
        'Mata Pelajaran > Matematika > Topik Pembelajaran III',
        style: GoogleFonts.poppins(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: AppColors.purple,
        ),
      ),
    );
  }
}

class TopNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.asset(
          'lib/icons/logo.svg',
          width: 32,
        ),
        Row(
          children: [
            Container(
              padding: EdgeInsets.all(6),
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(6),
              ),
              child: SvgPicture.asset(
                'lib/icons/paper.svg',
                width: 36,
              ),
            ),
            Container(
              padding: EdgeInsets.all(6),
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(6),
              ),
              child: SvgPicture.asset(
                'lib/icons/new-notifications.svg',
                width: 36,
              ),
            ),
            Container(
              padding: EdgeInsets.all(6),
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(6),
              ),
              child: SvgPicture.asset(
                'lib/icons/settings-cog.svg',
                width: 36,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class Soal {
  final String soal;
  final List<String> opsi;
  String? jawaban;
  bool notSure = false;

  Soal({
    required this.soal,
    required this.opsi,
    this.jawaban,
  });
}

final List<Soal> mySoal = [
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal:
        'Berapakah hasil dari 5 x 3 Berapakah hasil dari 5 x 3 Berapakah hasil dari 5 x 3 Berapakah hasil dari 5 x 3 Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 5 x 3?',
    opsi: ['10', '12', '15', '18', '20'],
  ),
  Soal(
    soal: 'Berapakah hasil dari 2 + 2?',
    opsi: ['4', '5', '6', '7', '8'],
  ),
];

class SoalPageViewBuilder extends StatefulWidget {
  final List<Soal> mySoal;

  SoalPageViewBuilder({required this.mySoal});

  @override
  _SoalPageViewBuilderState createState() => _SoalPageViewBuilderState();
}

class MultiRowBottomNavigationBar extends StatelessWidget {
  final int currentIndex;
  final Function(int) onTap;
  final List<BottomNavigationBarItem> items;
  final int itemsPerRow;
  final double itemSpacing; // add an itemSpacing property
  final List<bool> completed; // add a completed property
  final List<bool> notSure; // add a notSure property

  MultiRowBottomNavigationBar({
    required this.currentIndex,
    required this.onTap,
    required this.items,
    required this.completed, // make the completed property required
    required this.notSure, // make the notSure property required
    this.itemsPerRow = 5,
    this.itemSpacing = 8, // set a default value for itemSpacing
  });

  @override
  Widget build(BuildContext context) {
    final rows = <Widget>[];
    for (var i = 0; i < items.length; i += itemsPerRow) {
      rows.add(
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: items
                .skip(i)
                .take(itemsPerRow)
                .map(
                  (item) => Stack(
                    children: [
                      GestureDetector(
                        onTap: () => onTap(items.indexOf(item)),
                        child: Center(child: item.icon),
                      ),
                      if (completed[items.indexOf(item)])
                        Positioned(
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Container(
                            height: 50 * 0.25,
                            decoration: BoxDecoration(
                              color: AppColors.purple,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(8),
                                bottomRight: Radius.circular(8),
                              ),
                            ),
                          ),
                        ),
                      if (notSure[items.indexOf(item)])
                        Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            width: 50 * 0.30,
                            height: 50 * 0.30,
                            decoration: BoxDecoration(
                              color: Color(0xffEF6262),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8),
                                bottomRight: Radius.circular(4),
                                bottomLeft: Radius.circular(4),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      );
    }
    return Column(
      children: rows.map((row) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: itemSpacing / 2),
          child: row,
        );
      }).toList(),
    );
  }
}

class _SoalPageViewBuilderState extends State<SoalPageViewBuilder> {
  PageController _pageController = PageController();
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    int progressValue =
        (widget.mySoal.where((soal) => soal.jawaban != null).length /
                widget.mySoal.length *
                100)
            .round();
    return Column(
      children: [
        SizedBox(
          // insertValue
          height: 400,
          child: PageView.builder(
            controller: _pageController,
            itemCount: widget.mySoal.length,
            itemBuilder: (context, index) {
              return SoalCard(
                soal: widget.mySoal[index],
                soalIndex: index,
                onPageChanged: (selectedIndex, selectedJawaban, notSure) {
                  setState(() {
                    widget.mySoal[selectedIndex].jawaban = selectedJawaban;
                    widget.mySoal[selectedIndex].notSure = notSure;
                  });
                },
              );
            },
            onPageChanged: (index) {
              setState(() {
                _currentPage = index;
              });
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(right: 16),
                child: ElevatedButton(
                  onPressed: _currentPage == 0
                      ? null
                      : () {
                          _pageController.previousPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOut,
                          );
                        },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: _currentPage == 0
                        ? Color(0xffDBDFEA)
                        : AppColors.purple,
                    foregroundColor: AppColors.white,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  child: Text(
                    'Prev',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 16),
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      widget.mySoal[_currentPage].notSure =
                          !widget.mySoal[_currentPage].notSure;
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: widget.mySoal[_currentPage].notSure
                        ? Color(0xffEF6262)
                        : AppColors.white,
                    foregroundColor: widget.mySoal[_currentPage].notSure
                        ? AppColors.white
                        : AppColors.purple,
                    elevation: 0,
                  ),
                  child: Text(
                    'Not Sure',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: _currentPage == widget.mySoal.length - 1
                    ? null
                    : () {
                        _pageController.nextPage(
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInOut,
                        );
                      },
                style: ElevatedButton.styleFrom(
                  backgroundColor: _currentPage == widget.mySoal.length - 1
                      ? Color(0xffDBDFEA)
                      : AppColors.purple,
                  foregroundColor: AppColors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
                child: Text(
                  'Next',
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 16),
        Center(
          child: Text(
            "Soal ${_currentPage + 1}/${widget.mySoal.length}",
            style:
                GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          decoration: BoxDecoration(
              color: AppColors.white, borderRadius: BorderRadius.circular(24)),
          child: MultiRowBottomNavigationBar(
            currentIndex: _currentPage,
            onTap: (index) {
              _pageController.jumpToPage(index);
            },
            items: List.generate(
              widget.mySoal.length,
              (index) => BottomNavigationBarItem(
                label: '',
                icon: Container(
                  width: 50, // set a fixed width
                  height: 50, // set a fixed height
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: index == _currentPage
                        ? AppColors.purple
                        : Color(0xffF1F1F1),
                  ),
                  child: Text(
                    "${index + 1}",
                    style: GoogleFonts.poppins(
                      color: index == _currentPage
                          ? AppColors.white
                          : AppColors.purple,
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
            completed: List.generate(
              widget.mySoal.length,
              (index) => widget.mySoal[index].jawaban != null,
            ),
            notSure: List.generate(
              widget.mySoal.length,
              (index) => widget.mySoal[index].notSure,
            ),
          ),
        ),

        // Move the progress bar and "Submit" button below the pagination
        SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Progress',
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    color: AppColors.purple,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 150,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: LinearProgressIndicator(
                          minHeight: 10,
                          backgroundColor: AppColors.white,
                          valueColor: AlwaysStoppedAnimation(AppColors.purple),
                          value: widget.mySoal
                                  .where((soal) => soal.jawaban != null)
                                  .length /
                              widget.mySoal.length,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 8),
                      child: Text(
                        '$progressValue%',
                        style: GoogleFonts.poppins(
                          fontSize: 18,
                          color: AppColors.purple,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 16),
            SizedBox(
              width: 120,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.purple,
                  foregroundColor: AppColors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
                onPressed: () {
                  // TODO: implement submit logic
                },
                child: Text(
                  'Submit',
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class SoalCard extends StatefulWidget {
  final Soal soal;
  final int soalIndex;
  final Function(int, String?, bool) onPageChanged;

  SoalCard(
      {required this.soal,
      required this.soalIndex,
      required this.onPageChanged});

  @override
  _SoalCardState createState() => _SoalCardState();
}

class _SoalCardState extends State<SoalCard> {
  String? _selectedAnswer;
  bool _notSure = false;

  String limitString(String inputString, int maxLength) {
    if (inputString.length <= maxLength) {
      return inputString;
    } else {
      return inputString.substring(0, maxLength) + '...';
    }
  }

  @override
  void initState() {
    super.initState();
    _selectedAnswer = widget.soal.jawaban;
    _notSure = widget.soal.notSure;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(24),
        color: AppColors.white,
      ),
      margin: EdgeInsets.only(top: 24),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          // GPTMARKER
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              limitString(widget.soal.soal, 24),
              style: GoogleFonts.poppins(
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: AppColors.purple,
              ),
            ),
            ...widget.soal.opsi.map(
              (answer) => InkWell(
                onTap: () {
                  setState(() {
                    if (_selectedAnswer == answer) {
                      _selectedAnswer = null;
                    } else {
                      _selectedAnswer = answer;
                    }
                    widget.onPageChanged(
                      widget.soalIndex,
                      _selectedAnswer,
                      widget.soal
                          .notSure, // pass the current notSure state of the question
                    );
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 1440),
                  child: Container(
                    width: 540,
                    margin: EdgeInsets.symmetric(vertical: 6),
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: _selectedAnswer == answer
                          ? AppColors.purple
                          : Colors.white,
                      border: Border.all(
                        color: AppColors.purple,
                      ),
                    ),
                    child: Text(
                      limitString(answer, 30),
                      style: GoogleFonts.poppins(
                        fontSize: 20,
                        color: _selectedAnswer == answer
                            ? AppColors.white
                            : AppColors.purple,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
